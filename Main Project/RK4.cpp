#include "RK4.h"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <numeric> 
#include <chrono>
#include <random>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

void RK4::set_max_iterations(int maxiterations)
{
	_max_iterations = maxiterations;
}

double RK4::tau(double dt, double n)
{
	return dt*n;
}

double RK4::f(double _dX)
{
	return _dX;
}

double RK4::g(double _X, double _dX, double Q)
{
	return -(_dX / Q + _X);
}

double RK4::g(double _X, double _dX, double Q, double _Z0)
{
	return -(_dX / Q + _X) + _Z0; 
}

double RK4::g(double _X, double _dX, double Q, double dt, double n, double additional_factor, double natural_frequency, double driving_frequency, double eta)
{
	_delta = driving_frequency / natural_frequency;
	return -(_dX / Q + _X) + eta * sin((_delta * (tau(dt, n) + additional_factor)));
}

double RK4::g(double _X, double _dX, double Q, double Z0, double dt, double n, double additional_factor, double natural_frequency, double driving_frequency, double eta)
{
	_delta = driving_frequency / natural_frequency;
	return -(_dX / Q + _X) + eta * sin((_delta * (tau(dt, n) + additional_factor))) + Z0;
}

double RK4::g_NL(double _X, double _dX, double Q, double Z0, double dt, double n, double additional_factor, double natural_frequency, double driving_frequency, double eta)
{
	_delta = driving_frequency / natural_frequency;
	return -(_dX / Q + sin(_X)) + eta * sin((_delta * (tau(dt, n) + additional_factor))) + Z0;
}

void RK4::simulate_oscillations(double Q, double dt) //add max iterations as argument
{
	_x.clear();
	_X.clear();
	_dX.clear();
	_x.push_back(1);
	_X.push_back(1);
	_dX.push_back(0);

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_OSCILLATIONS_DATA.txt";
	ofstream oscillations_file;
	oscillations_file.open(ss.str().c_str());

	for (int n = 0; n <= _max_iterations; n++)
	{
		_k1 = dt * f(_dX.at(n));
		_l1 = dt * g(_X.at(n), _dX.at(n), Q);
		_k2 = dt * f((_dX.at(n) + 0.5*_l1));
		_l2 = dt * g((_X.at(n) + 0.5*_k1), (_dX.at(n) + 0.5*_l1), Q);
		_k3 = dt * f((_dX.at(n) + 0.5*_l2));
		_l3 = dt * g((_X.at(n) + 0.5*_k2), (_dX.at(n) + 0.5*_l2), Q);
		_k4 = dt * f((_dX.at(n) + _l3));
		_l4 = dt * g((_X.at(n) + _k3), (_dX.at(n) + _l3), Q);

		_X.push_back((_X.at(n) + (_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6)));
		_dX.push_back((_dX.at(n) + (_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6)));

		_x.push_back(exp(-tau(n, dt) / (2 * Q))*(cos(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2))))));

		oscillations_file << tau(dt, n) << "\t" << _X.at(n) << "\t" << _dX.at(n) << "\t" << _x.at(n) << endl;

	}
	oscillations_file.close();

}

void RK4::simulate_energy(double Q, double dt)
{
	_x.clear();
	_X.clear();
	_dX.clear();
	_x.push_back(1);
	_X.push_back(1);
	_dX.push_back(0);

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_ENERGY_DATA.txt";
	ofstream energy_file;
	energy_file.open(ss.str().c_str());

	for (int n = 0; n <= _max_iterations; n++)
	{
		_k1 = dt * f(_dX.at(n));
		_l1 = dt * g(_X.at(n), _dX.at(n), Q);
		_k2 = dt * f((_dX.at(n) + 0.5*_l1));
		_l2 = dt * g((_X.at(n) + 0.5*_k1), (_dX.at(n) + 0.5*_l1), Q);
		_k3 = dt * f((_dX.at(n) + 0.5*_l2));
		_l3 = dt * g((_X.at(n) + 0.5*_k2), (_dX.at(n) + 0.5*_l2), Q);
		_k4 = dt * f((_dX.at(n) + _l3));
		_l4 = dt * g((_X.at(n) + _k3), (_dX.at(n) + _l3), Q);

		_X.push_back((_X.at(n) + (_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6)));
		_dX.push_back((_dX.at(n) + (_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6)));
		//x.push_back(_X.at(0) * exp(-(tau(n, dt)) / (2 * Q)) * (cos(tau(n, dt) * (sqrt(1 - (1 / (4 * pow(Q, 2)))))))); //old x

		_x.push_back(exp(-tau(n, dt) / (2 * Q))*(cos(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2))))));


		energy_file << tau(dt, n) << "\t" << pow(_X.at(n), 2) + pow(_dX.at(n), 2) << endl;

	}
	energy_file.close();
}

void RK4::simulate_brownian_motion(double Q, double dt)
{
	_Z0.clear();

	_X.clear();
	_dX.clear();
	
	_X.push_back(0);     //Starting value for X
	_dX.push_back(0);    //Starting value for dX
	
	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_NOISE_DATA_" << double(Q) << ".txt";
	ofstream noise_file;
	noise_file.open(ss.str().c_str());
	
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);
	normal_distribution<double> distribution(0, 1.0/3.0);

	for (int n = 0; n <= _max_iterations; ++n)
	{

		_Z0.push_back(distribution(generator)); //N(tau)

		_k1 = dt * f(_dX.at(n));
		_l1 = dt * g(_X.at(n), _dX.at(n), Q, _Z0.at(n));
		_k2 = dt * f((_dX.at(n) + 0.5*_l1));
		_l2 = dt * g((_X.at(n) + 0.5*_k1), (_dX.at(n) + 0.5*_l1), Q, _Z0.at(n));
		_k3 = dt * f((_dX.at(n) + 0.5*_l2));
		_l3 = dt * g((_X.at(n) + 0.5*_k2), (_dX.at(n) + 0.5*_l2), Q, _Z0.at(n));
		_k4 = dt * f((_dX.at(n) + _l3));
		_l4 = dt * g((_X.at(n) + _k3), (_dX.at(n) + _l3), Q, _Z0.at(n));

		_X.push_back((_X.at(n) + (_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6)));
		_dX.push_back((_dX.at(n) + (_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6)));
	
		noise_file << tau(dt, n) << "\t" << _X.at(n) << endl;
	}
	noise_file.close();
}

void RK4::calculate_X_standard_deviation(double Q, double dt)
{
	_Z0.clear();
	_sum_for_sd.clear();

	_X.clear();
	_dX.clear();

	_X.push_back(0);     //Starting value for X
	_dX.push_back(0);    //Starting value for dX

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_X_STANDARD_DEVIATION.txt";
	ofstream noise_file;
	noise_file.open(ss.str().c_str(), ofstream::app);

	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);
	normal_distribution<double> distribution(0, 1.0 / 3.0);

	for (int n = 0; n <= _max_iterations; ++n)
	{

		_Z0.push_back(distribution(generator)); //N(tau)

		_k1 = dt * f(_dX.at(n));
		_l1 = dt * g(_X.at(n), _dX.at(n), Q, _Z0.at(n));
		_k2 = dt * f((_dX.at(n) + 0.5*_l1));
		_l2 = dt * g((_X.at(n) + 0.5*_k1), (_dX.at(n) + 0.5*_l1), Q, _Z0.at(n));
		_k3 = dt * f((_dX.at(n) + 0.5*_l2));
		_l3 = dt * g((_X.at(n) + 0.5*_k2), (_dX.at(n) + 0.5*_l2), Q, _Z0.at(n));
		_k4 = dt * f((_dX.at(n) + _l3));
		_l4 = dt * g((_X.at(n) + _k3), (_dX.at(n) + _l3), Q, _Z0.at(n));

		_X.push_back((_X.at(n) + (_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6)));
		_dX.push_back((_dX.at(n) + (_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6)));

		if (n == _max_iterations)
		{
			_mean = accumulate(_X.begin(), _X.end(), double(0)) / _max_iterations;
			for (int i = 0; i <= _X.size(); i++)
				_sum_for_sd.push_back(pow(_X.at(n) - _mean, 2));
			
			double sum = accumulate(_sum_for_sd.begin(), _sum_for_sd.end(), double(0));
			_standard_deviation = (sum / _max_iterations);
			noise_file << _standard_deviation << "\t" << Q << endl;
		}
	}
	noise_file.close();
}

void RK4::calculate_Noise_standard_deviation(double Q, double dt)
{
	_Z0.clear();

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_NOISE_STANDARD_DEVIATION.txt";
	ofstream noise_file;
	noise_file.open(ss.str().c_str(), ofstream::app);

	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);
	normal_distribution<double> distribution(0, 1.0 / 3.0);

	for (int n = 0; n <= _max_iterations; ++n)
	{

		_Z0.push_back(pow(distribution(generator), 2)); //N(tau)

		if (n == _max_iterations)
		{
			double sum = accumulate(_Z0.begin(), _Z0.end(), double(0));
			_standard_deviation = (sum / _max_iterations);
			noise_file << _standard_deviation << "\t" << Q << endl;
		}
	}
	noise_file.close();
}

void RK4::calculate_error(double Q, double dt, double start_value, bool save_alternate_file)
{
	_x.clear();
	_x_squared.clear();
	_X.clear();
	_dX.clear();
	_x.push_back(start_value);
	_x_squared.push_back(start_value * start_value);
	_X.push_back(start_value);
	_dX.push_back(0);

	_Err.clear();

	if (save_alternate_file == false)
	{
		stringstream ss;
		ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_ERROR_DATA_Q" << double(Q) << ".txt";
		ofstream errors_file;
		errors_file.open(ss.str().c_str(), ofstream::app);

		for (int n = 0; n <= _max_iterations; n++)
		{
			_k1 = dt * f(_dX.at(n));
			_l1 = dt * g(_X.at(n), _dX.at(n), Q);
			_k2 = dt * f((_dX.at(n) + 0.5*_l1));
			_l2 = dt * g((_X.at(n) + 0.5*_k1), (_dX.at(n) + 0.5*_l1), Q);
			_k3 = dt * f((_dX.at(n) + 0.5*_l2));
			_l3 = dt * g((_X.at(n) + 0.5*_k2), (_dX.at(n) + 0.5*_l2), Q);
			_k4 = dt * f((_dX.at(n) + _l3));
			_l4 = dt * g((_X.at(n) + _k3), (_dX.at(n) + _l3), Q);

			_X.push_back((_X.at(n) + (_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6)));
			_dX.push_back((_dX.at(n) + (_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6)));
			_x.push_back(exp(-tau(n, dt) / (2 * Q))*(cos(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2))))));
			_x_squared.push_back(pow(exp(-tau(dt, n) / (2 * Q))*(cos(tau(dt, n)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(dt, n)*sqrt(1 - (1 / pow((2 * Q), 2))))), 2));
			_Err.push_back(pow((_x.at(n) - _X.at(n)), 2));

			if (n == _max_iterations)
			{
				_Err_total = accumulate(_Err.begin(), _Err.end(), double(0));
				_x_squared_total = accumulate(_x_squared.begin(), _x_squared.end(), double(0));
				_Relative_error_mean = (_Err_total / _x_squared_total);
				errors_file << _Relative_error_mean << "\t" << dt << endl;
			}

		}
		errors_file.close();
	}

	else
	{
		stringstream ss;
		ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_ERROR_DATA_Q" << double(Q) << "_X" << double(start_value) << ".txt";
		ofstream errors_file;
		errors_file.open(ss.str().c_str(), ofstream::app);

		for (int n = 0; n <= _max_iterations; n++)
		{
			_k1 = dt * f(_dX.at(n));
			_l1 = dt * g(_X.at(n), _dX.at(n), Q);
			_k2 = dt * f((_dX.at(n) + 0.5*_l1));
			_l2 = dt * g((_X.at(n) + 0.5*_k1), (_dX.at(n) + 0.5*_l1), Q);
			_k3 = dt * f((_dX.at(n) + 0.5*_l2));
			_l3 = dt * g((_X.at(n) + 0.5*_k2), (_dX.at(n) + 0.5*_l2), Q);
			_k4 = dt * f((_dX.at(n) + _l3));
			_l4 = dt * g((_X.at(n) + _k3), (_dX.at(n) + _l3), Q);

			_X.push_back((_X.at(n) + (_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6)));
			_dX.push_back((_dX.at(n) + (_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6)));
			_x.push_back(exp(-tau(n, dt) / (2 * Q))*(cos(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, dt)*sqrt(1 - (1 / pow((2 * Q), 2))))));
			_x_squared.push_back(pow(exp(-tau(dt, n) / (2 * Q))*(cos(tau(dt, n)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(dt, n)*sqrt(1 - (1 / pow((2 * Q), 2))))), 2));
			_Err.push_back(pow((_x.at(n) - _X.at(n)), 2));

			if (n == _max_iterations)
			{
				_Err_total = accumulate(_Err.begin(), _Err.end(), double(0));
				_x_squared_total = accumulate(_x_squared.begin(), _x_squared.end(), double(0));
				_Relative_error_mean = (_Err_total / _x_squared_total);
				errors_file << _Relative_error_mean << "\t" << dt << endl;
			}

		}
		errors_file.close();
	}

}

void RK4::driving_data_for_Bode_Plot(double Q)
{
	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/ANALYTICAL_DRIVING_BODE_PLOT_DATA.txt";
	ofstream driving_file;
	driving_file.open(ss.str().c_str());

	for (double D = 0.1; D < 11; D = D + 0.1)
	{
		_delta = D;
		_driving_amplitude = 1 / sqrt(pow((1 - _delta*_delta), 2) + pow(_delta / Q, 2));
		driving_file << _driving_amplitude << "\t" << _delta << endl;
	}

	driving_file.close();

}

void RK4::analytical_driving(double Q, double dt, double natural_frequency, double driving_frequency, double eta)
{
	//analytical so use _x

	_x.clear();
	_x.push_back(1);

	_w0 = natural_frequency;
	_wd = driving_frequency;
	_eta = eta;
	_delta = _wd / _w0;


	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/ANALYTICAL_DRIVING_DATA" << "_DELTA_" << double(_delta) << ".txt";
	ofstream driving_file;
	driving_file.open(ss.str().c_str());

	double dphi = dt * 2 * M_PI;

	_phase = atan((1 - _delta*_delta)*Q / _delta);
	double A = eta / (_delta*_delta*sin(_phase) - (_delta / Q)*cos(_phase) - sin(_phase));

	_C_1 = 1 - A*cos(_phase);
	_C_2 = (_C_1 / (2 * Q) + A*_delta*sin(_phase)) / sqrt(1 - 1 / (4 * Q*Q));

	for (int n = 0; n <= _max_iterations; n++)
	{
		_transient_solution = exp(-tau(n, dphi) / (2 * Q)) * (_C_1 * cos(tau(n, dphi)*sqrt(1 - (1 / pow((2 * Q), 2)))) + _C_2 * sin(tau(n, dphi)*sqrt(1 - (1 / pow((2 * Q), 2)))));
		_steady_state_solution = A*cos(_delta*tau(dphi, n) + _phase);
		_full_solution = _transient_solution + _steady_state_solution;
		_x.push_back(_full_solution);
		driving_file << tau(dphi, n) << "\t" << _x.at(n) << endl;
	}
	driving_file.close();
}

void RK4::simulate_driving(double Q, double dt, double natural_frequency, double driving_frequency, double eta)
{
	_X.clear();
	_dX.clear();
	_X.push_back(1);
	_dX.push_back(0);

	_w0 = natural_frequency;	
	_wd = driving_frequency;
	_delta = _wd / _w0;
	
	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_DRIVING_DATA_DELTA_" << double(_delta) << ".txt";
	ofstream oscillations_file;
	oscillations_file.open(ss.str().c_str());

	double dphi = dt * 2 * M_PI;

	for (int n = 0; n <= _max_iterations; n++)
	{
		_k1 = f(_dX.at(n));
		_l1 = g(_X.at(n), _dX.at(n), Q, dphi, n, 0.0, natural_frequency, driving_frequency, eta);
		_k2 = f((_dX.at(n) + 0.5 * dphi * _l1));
		_l2 = g((_X.at(n) + 0.5 * dphi * _k1), (_dX.at(n) + 0.5*dphi * _l1), Q, dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k3 = f((_dX.at(n) + 0.5*dphi * _l2));
		_l3 = g((_X.at(n) + 0.5*dphi * _k2), (_dX.at(n) + 0.5*dphi * _l2), Q, dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k4 = f((_dX.at(n) + dphi * _l3));
		_l4 = g((_X.at(n) + dphi * _k3), (_dX.at(n) + dphi * _l3), Q, dphi, n, dphi, natural_frequency, driving_frequency, eta);

		_X.push_back((_X.at(n) + dphi * ((_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6))));
		_dX.push_back((_dX.at(n) + dphi *	((_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6))));
	
		oscillations_file << tau(dphi, n) << "\t" << _X.at(n) << "\t" << _dX.at(n) << endl;

	}
	oscillations_file.close();

}

void RK4::calculate_driving_error(double Q, double dt, double natural_frequency, double driving_frequency, double eta)
{
	//Use the following paths to investigate error as a function of stepsize or delta
	//REMEMBER TO CHANGE WHAT IS BEING SAVED, I.E: EITHER STEP SIZE OR DELTA

	stringstream ss;
	//ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/DRIVE_ERROR_AS_A_FUNCTION_OF_STEPSIZE.txt";
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/DRIVE_ERROR_AS_A_FUNCTION_OF_DELTA.txt";
	ofstream errors_file;
	errors_file.open(ss.str().c_str(), ofstream::app);

	_X.clear();
	_dX.clear();
	_X.push_back(1);
	_dX.push_back(0);

	_x.clear();
	_x_drive_squared.clear();
	_x.push_back(1);
	_x_drive_squared.push_back(1);
	
	_err_drive.clear();

	_eta = eta;
	_w0 = natural_frequency;
	_wd = driving_frequency;
	_delta = _wd / _w0;
	
	double dphi = dt * 2 * M_PI;

	_phase = atan((1 - _delta*_delta)*Q / _delta);
	double A = eta / (_delta*_delta*sin(_phase) - (_delta / Q)*cos(_phase) - sin(_phase));

	_C_1 = 1 - A*cos(_phase);
	_C_2 = (_C_1 / (2 * Q) + A*_delta*sin(_phase)) / sqrt(1 - 1 / (4 * Q*Q));

	for (int n = 0; n <= _max_iterations; n++)
	{
		_k1 = f(_dX.at(n));
		_l1 = g(_X.at(n), _dX.at(n), Q, dphi, n, 0.0, natural_frequency, driving_frequency, eta);
		_k2 = f((_dX.at(n) + 0.5 * dphi * _l1));
		_l2 = g((_X.at(n) + 0.5 * dphi * _k1), (_dX.at(n) + 0.5*dphi * _l1), Q, dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k3 = f((_dX.at(n) + 0.5*dphi * _l2));
		_l3 = g((_X.at(n) + 0.5*dphi * _k2), (_dX.at(n) + 0.5*dphi * _l2), Q, dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k4 = f((_dX.at(n) + dphi * _l3));
		_l4 = g((_X.at(n) + dphi * _k3), (_dX.at(n) + dphi * _l3), Q, dphi, n, dphi, natural_frequency, driving_frequency, eta);

		_X.push_back((_X.at(n) + dphi * ((_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6))));
		_dX.push_back((_dX.at(n) + dphi *	((_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6))));

		_transient_solution = exp(-tau(n, dphi) / (2 * Q)) * (_C_1 * cos(tau(n, dphi)*sqrt(1 - (1 / pow((2 * Q), 2)))) + _C_2 * sin(tau(n, dphi)*sqrt(1 - (1 / pow((2 * Q), 2)))));
		_steady_state_solution = A*cos(_delta*tau(dphi, n) + _phase);
		_full_solution = _transient_solution + _steady_state_solution;
		_x.push_back(_full_solution);

		_err_drive.push_back(pow(_x.at(n) - _X.at(n), 2));
		_x_drive_squared.push_back(_full_solution*_full_solution);

		if (n == _max_iterations)
		{
			_err_drive_total = accumulate(_err_drive.begin(), _err_drive.end(), double(0));
			_x_drive_squared_total = accumulate(_x_drive_squared.begin(), _x_drive_squared.end(), double(0));

			_rel_err_drive = _err_drive_total / _x_drive_squared_total;
			errors_file << _rel_err_drive << "\t" << _delta << endl;
		}

	}
	errors_file.close();
}

void RK4::simulate_driving_with_noise(double Q, double dt, double natural_frequency, double driving_frequency, double eta)
{
	_Z0.clear();

	_X.clear();
	_dX.clear();
	_X.push_back(1);
	_dX.push_back(0);

	_w0 = natural_frequency;
	_wd = driving_frequency;
	_delta = _wd / _w0;

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_DRIVING_NOISE_DATA_DELTA_" << double(_delta) << ".txt";
	ofstream oscillations_file;
	oscillations_file.open(ss.str().c_str());

	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);
	normal_distribution<double> distribution(0, 1.0 / 3.0);

	double dphi = dt * 2 * M_PI;

	for (int n = 0; n <= _max_iterations; n++)
	{
		_Z0.push_back(distribution(generator)); //N(tau)

		_k1 = f(_dX.at(n));
		_l1 = g(_X.at(n), _dX.at(n), Q, _Z0.at(n), dphi, n, 0.0, natural_frequency, driving_frequency, eta);
		_k2 = f((_dX.at(n) + 0.5 * dphi * _l1));
		_l2 = g((_X.at(n) + 0.5 * dphi * _k1), (_dX.at(n) + 0.5*dphi * _l1), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k3 = f((_dX.at(n) + 0.5*dphi * _l2));
		_l3 = g((_X.at(n) + 0.5*dphi * _k2), (_dX.at(n) + 0.5*dphi * _l2), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k4 = f((_dX.at(n) + dphi * _l3));
		_l4 = g((_X.at(n) + dphi * _k3), (_dX.at(n) + dphi * _l3), Q, _Z0.at(n), dphi, n, dphi, natural_frequency, driving_frequency, eta);

		_X.push_back((_X.at(n) + dphi * ((_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6))));
		_dX.push_back((_dX.at(n) + dphi *	((_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6))));

		oscillations_file << tau(dphi, n) << "\t" << _X.at(n) << "\t" << _Z0.at(n) << endl;

	}
	oscillations_file.close();

}

void RK4::simulate_NL_driving_with_noise(double Q, double dt, double natural_frequency, double driving_frequency, double eta)
{
	_Z0.clear();
	_X.clear();
	_dX.clear();
	_X.push_back(1);
	_dX.push_back(0);

	_w0 = natural_frequency;
	_wd = driving_frequency;
	_delta = _wd / _w0;

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/RK4_NONLINEAR_DRIVEN_WITH_NOISE_ETA" << double(eta) << "_DELTA" << double(_delta) << ".txt";
	ofstream oscillations_file;
	oscillations_file.open(ss.str().c_str());

	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);
	normal_distribution<double> distribution(0, 1.0 / 3.0);

	double dphi = dt * 2 * M_PI;

	for (int n = 0; n <= _max_iterations; n++)
	{
		_Z0.push_back(distribution(generator)); //N(tau)

		_k1 = f(_dX.at(n));
		_l1 = g_NL(_X.at(n), _dX.at(n), Q, _Z0.at(n), dphi, n, 0.0, natural_frequency, driving_frequency, eta);
		_k2 = f((_dX.at(n) + 0.5 * dphi * _l1));
		_l2 = g_NL((_X.at(n) + 0.5 * dphi * _k1), (_dX.at(n) + 0.5*dphi * _l1), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k3 = f((_dX.at(n) + 0.5*dphi * _l2));
		_l3 = g_NL((_X.at(n) + 0.5*dphi * _k2), (_dX.at(n) + 0.5*dphi * _l2), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k4 = f((_dX.at(n) + dphi * _l3));
		_l4 = g_NL((_X.at(n) + dphi * _k3), (_dX.at(n) + dphi * _l3), Q, _Z0.at(n), dphi, n, dphi, natural_frequency, driving_frequency, eta);

		_X.push_back((_X.at(n) + dphi * ((_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6))));
		_dX.push_back((_dX.at(n) + dphi *	((_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6))));

		oscillations_file << tau(dphi, n) << "\t" << _X.at(n) << endl;

	}
	oscillations_file.close();
}

void RK4::detect_bifurcation(double Q, double dt, double natural_frequency, double driving_frequency, double eta)
{
	_Z0.clear();
	_X.clear();
	_dX.clear();
	_X.push_back(1);
	_dX.push_back(0);

	_w0 = natural_frequency;
	_wd = driving_frequency;
	_delta = _wd / _w0;

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/BIFURCATION_DETECTIONS.txt";
	ofstream oscillations_file;
	oscillations_file.open(ss.str().c_str(), ofstream::app);

	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);
	normal_distribution<double> distribution(0, 1.0 / 3.0);

	double dphi = dt * 2 * M_PI;
	//vector<double> _sum_for_RMS;
	//double RMS;

	for (int n = 0; n <= _max_iterations; n++)
	{
		_Z0.push_back(distribution(generator)); //N(tau)

		_k1 = f(_dX.at(n));
		_l1 = g_NL(_X.at(n), _dX.at(n), Q, _Z0.at(n), dphi, n, 0.0, natural_frequency, driving_frequency, eta);
		_k2 = f((_dX.at(n) + 0.5 * dphi * _l1));
		_l2 = g_NL((_X.at(n) + 0.5 * dphi * _k1), (_dX.at(n) + 0.5*dphi * _l1), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k3 = f((_dX.at(n) + 0.5*dphi * _l2));
		_l3 = g_NL((_X.at(n) + 0.5*dphi * _k2), (_dX.at(n) + 0.5*dphi * _l2), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
		_k4 = f((_dX.at(n) + dphi * _l3));
		_l4 = g_NL((_X.at(n) + dphi * _k3), (_dX.at(n) + dphi * _l3), Q, _Z0.at(n), dphi, n, dphi, natural_frequency, driving_frequency, eta);

		_X.push_back((_X.at(n) + dphi * ((_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6))));
		_dX.push_back((_dX.at(n) + dphi *	((_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6))));
		
		//_sum_for_RMS.push_back(_X.at(n) * _X.at(n));
		//RMS = sqrt(accumulate(_sum_for_RMS.begin(), _sum_for_RMS.end(), double(0)) / n);
		
		if (_X.at(n) > _X.at(0) * 4)
		{
			oscillations_file << 1 << "\t" << _delta << "\t" << eta << "\t" << _X.at(n) << "\t" << tau(n, dphi) << endl;
			break;
		}
		else
		{
			if (n == _max_iterations)
				oscillations_file << 0 << "\t" << _delta << "\t" << eta << "\t" << 0 << "\t" << 0 << endl; //will discard 0s in python but need them to keep array filled
		}

	}
	oscillations_file.close();
}

void RK4::calculate_probability_of_bifurcation(int max_runs, double Q, double dt, double natural_frequency, double driving_frequency, double eta)
{
	cout << driving_frequency << endl;

	stringstream ss;
	ss << "C:/Users/Petar/Google Drive/Uni/Uni Work/Year 3/SONLR/Main Project/Main Project/DATA/BIFURCATION_PROBABILITY_ETA" << double(eta) << ".txt";
	ofstream oscillations_file;
	oscillations_file.open(ss.str().c_str(), ofstream::app);

	_bifurcation.clear();
	_no_bifurcation.clear();

	for (int i = 0; i <= max_runs; i++)
	{
		_Z0.clear();
		_X.clear();
		_dX.clear();
		_X.push_back(1);
		_dX.push_back(0);

		_w0 = natural_frequency;
		_wd = driving_frequency;
		_delta = _wd / _w0;

		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
		default_random_engine generator(seed);
		normal_distribution<double> distribution(0, 1.0 / 3.0);

		double dphi = dt * 2 * M_PI;

		for (int n = 0; n <= _max_iterations; n++)
		{
			_Z0.push_back(distribution(generator)); //N(tau)

			_k1 = f(_dX.at(n));
			_l1 = g_NL(_X.at(n), _dX.at(n), Q, _Z0.at(n), dphi, n, 0.0, natural_frequency, driving_frequency, eta);
			_k2 = f((_dX.at(n) + 0.5 * dphi * _l1));
			_l2 = g_NL((_X.at(n) + 0.5 * dphi * _k1), (_dX.at(n) + 0.5*dphi * _l1), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
			_k3 = f((_dX.at(n) + 0.5*dphi * _l2));
			_l3 = g_NL((_X.at(n) + 0.5*dphi * _k2), (_dX.at(n) + 0.5*dphi * _l2), Q, _Z0.at(n), dphi, n, dphi / 2.0, natural_frequency, driving_frequency, eta);
			_k4 = f((_dX.at(n) + dphi * _l3));
			_l4 = g_NL((_X.at(n) + dphi * _k3), (_dX.at(n) + dphi * _l3), Q, _Z0.at(n), dphi, n, dphi, natural_frequency, driving_frequency, eta);

			_X.push_back((_X.at(n) + dphi * ((_k1 / 6) + (_k2 / 3) + (_k3 / 3) + (_k4 / 6))));
			_dX.push_back((_dX.at(n) + dphi *	((_l1 / 6) + (_l2 / 3) + (_l3 / 3) + (_l4 / 6))));

			if (_X.at(n) > _X.at(0) * 4)
			{
				_bifurcation.push_back(1);
				break;
			}
			else
			{
				if (n == _max_iterations)
					_no_bifurcation.push_back(0);
			}
		}
	}

	_probability = double(_bifurcation.size()) / double(_bifurcation.size() + _no_bifurcation.size());
	oscillations_file << _probability << "\t" << _delta << endl;
	oscillations_file.close();
}