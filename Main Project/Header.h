#include <iostream>
#include <cmath>
#include <vector>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <numeric> 
#include <chrono>
#include <random>


using namespace std;
double tau0 = 0;
double PI = 3.14159265359;
int N = 100000; //this is max n.  

//Comparison & Energy
vector<double> X(0);
vector<double> dX(0);
vector<double> x(0);
vector<double> x_squared(0);
vector<double> XEuler(0);
vector<double> dXEuler(0);
vector<double> xEuler(0);

//Error
vector<double> ErrEuler(0);
vector<double> Err(0);

//Brownian Motion
vector<double> Z0(0), U1(0), U2(0);

double tau(double n, double deltatau) //This method gives the time location of the system
{
	return deltatau*n; 
}
double f(double dX) //This method returns the derivative of X, unsimplified due to possible evolution.
{
	return dX;
}
double g(double X, double dX, double Q) //This method returns the quantity associated with the force, unsimplified due to possible evolution.
{
	return -(dX / Q + X);
}
double g(double X, double dX, double Q, double Z0)//overloaded g for Brownian motion
{
	return -(dX / Q + X) + Z0;
} 

void clearcontents(double Q, double start_value)
{
	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/RK4_DATA.txt";
	ofstream oscillatorRK4file;
	oscillatorRK4file.open(ss.str().c_str(), fstream::trunc);
	oscillatorRK4file.close();

	//stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/EULER_DATA.txt";
	ofstream oscillatorEULERfile;
	oscillatorEULERfile.open(ss.str().c_str(), fstream::trunc);
	oscillatorEULERfile.close();

	for (Q = Q; Q <= 10000; Q = Q * 10)
	{
		stringstream em_E;
		em_E << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/EULER_ERRORMEAN_Q" << double(Q) << ".txt";
		ofstream EULER_ERRORMEANfile;
		EULER_ERRORMEANfile.open(em_E.str().c_str(), fstream::trunc);
		EULER_ERRORMEANfile.close();

		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(Q) << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), fstream::trunc);
		RK4_ERRORMEANfile.close();

		//stringstream ss;
		ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/ENERGY_RK4_DATA_Q" << double(Q) << ".txt";
		ofstream ENERGY_RK4file;
		ENERGY_RK4file.open(ss.str().c_str(), fstream::trunc);
		ENERGY_RK4file.close();
	}

	for (start_value = 1; start_value <= 4; start_value++)
	{
		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(30) << "_X" << start_value << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), ofstream::trunc);
		RK4_ERRORMEANfile.close();
	}

}

void RK4(double Q, double deltatau)
{
	x.clear(); //testing if clearing the vectors will fix Q automation
	X.clear();
	dX.clear();
	x.push_back(1);		//Starting value for ANALYTICAL
	X.push_back(1);     //Starting value for SIMULATION
	dX.push_back(0);    //Starting value for dX	

	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/RK4_DATA.txt";
	ofstream oscillatorRK4file;
	oscillatorRK4file.open(ss.str().c_str());

	double k1, l1, k2, l2, k3, l3, k4, l4;
	for (int n = 0; n <= N; n++)
	{
		k1 = deltatau * f(dX.at(n));
		l1 = deltatau * g(X.at(n), dX.at(n), Q);
		k2 = deltatau * f((dX.at(n) + 0.5*l1));
		l2 = deltatau * g((X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
		k3 = deltatau * f((dX.at(n) + 0.5*l2));
		l3 = deltatau * g((X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
		k4 = deltatau * f((dX.at(n) + l3));
		l4 = deltatau * g((X.at(n) + k3), (dX.at(n) + l3), Q);

		X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));
		dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));
		//x.push_back(X.at(0) * exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2)))))))); //old x
		
		x.push_back(exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))));

		
		oscillatorRK4file << tau(n, deltatau) << "\t" << X.at(n) << "\t" << dX.at(n) << "\t" << x.at(n) << endl;

	}
	oscillatorRK4file.close();
}

void Euler(double Q, double deltatau)
{
	xEuler.clear();
	XEuler.clear();
	dXEuler.clear();

	xEuler.push_back(2);	 //Starting value for ANALYTICAL
	XEuler.push_back(2);     //Starting value for SIMULATION
	dXEuler.push_back(0);    //Starting value for dX

	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/EULER_DATA.txt";
	ofstream oscillatorEULERfile;
	oscillatorEULERfile.open(ss.str().c_str());

	for (int n = 0; n <= N; ++n)
	{
		XEuler.push_back(XEuler.at(n) + dXEuler.at(n)*deltatau);
		dXEuler.push_back((-dXEuler.at(n) / Q - XEuler.at(n))*deltatau + dXEuler.at(n));
		x.push_back(x.at(0)*exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))));
		
		ErrEuler.push_back(abs(xEuler.at(n) - XEuler.at(n)));
		oscillatorEULERfile << n << "\t" << XEuler.at(n) << "\t" << dXEuler.at(n) << "\t" << xEuler.at(n) << endl;
	}
	oscillatorEULERfile.close();
}

void Error(double Q, double start_value, double deltatau, bool save_alternate_file) //start_value is to specify the starting value of X
{
	x.clear();
	x.shrink_to_fit();
	x_squared.clear();
	x_squared.shrink_to_fit();
	X.clear();
	X.shrink_to_fit();
	dX.clear();
	dX.shrink_to_fit();

	x.push_back(start_value);
	x_squared.push_back(pow(start_value, 2));
	X.push_back(start_value);
	dX.push_back(0);

	xEuler.clear();
	xEuler.shrink_to_fit();
	XEuler.clear();
	XEuler.shrink_to_fit();
	dXEuler.clear();
	dXEuler.shrink_to_fit();

	xEuler.push_back(start_value);
	XEuler.push_back(start_value);
	dXEuler.push_back(0);

	Err.clear();
	Err.shrink_to_fit();
	ErrEuler.clear();
	ErrEuler.shrink_to_fit();
	//Err.push_back(0);
	//ErrEuler.push_back(0);

	if (save_alternate_file == false)
	{
		//stringstream em_E;
		//em_E << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/EULER_ERRORMEAN_Q" << double(Q) << ".txt";
		//ofstream EULER_ERRORMEANfile;
		//EULER_ERRORMEANfile.open(em_E.str().c_str(), ofstream::app);

		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(Q) << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), ofstream::app);

		double errEuler, errtotEuler, Rel_errmeanEuler;
		double k1, l1, k2, l2, k3, l3, k4, l4, err, errtot, Rel_errmean, x_squared_tot;

		for (int n = 0; n <= N; ++n)
		{
			k1 = deltatau * f(dX.at(n));
			l1 = deltatau * g(X.at(n), dX.at(n), Q);
			k2 = deltatau * f((dX.at(n) + 0.5*l1));
			l2 = deltatau * g((X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
			k3 = deltatau * f((dX.at(n) + 0.5*l2));
			l3 = deltatau * g((X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
			k4 = deltatau * f((dX.at(n) + l3));
			l4 = deltatau * g((X.at(n) + k3), (dX.at(n) + l3), Q);

			X.push_back((X.at(n) + (k1 / 6.0) + (k2 / 3.0) + (k3 / 3.0) + (k4 / 6.0)));
			dX.push_back((dX.at(n) + (l1 / 6.0) + (l2 / 3.0) + (l3 / 3.0) + (l4 / 6.0)));
			x.push_back(exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))));
			x_squared.push_back(pow(exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1/sqrt(4*Q*Q - 1))*sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))), 2));
			Err.push_back(pow((x.at(n) - X.at(n)), 2));
			
			//x.push_back(X.at(0) * exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));
			//x_squared.push_back(pow((X.at(0) * exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2)))))))), 2));
			//Err.push_back(pow(((X.at(0) * exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2)))))))) - (X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6))), 2));
			//Err.push_back(pow(((X.at(0) * exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2)))))))) - X.at(n)), 2)); //lastly using this one
			

			//XEuler.push_back(XEuler.at(n) + dXEuler.at(n)*deltatau);
			//dXEuler.push_back((-dXEuler.at(n) / Q - XEuler.at(n))*deltatau + dXEuler.at(n));
			//xEuler.push_back(X.at(0) * exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));
			//ErrEuler.push_back(pow(xEuler.at(n) - XEuler.at(n), 2));

			if (n == N)
			{
				errtot = accumulate(Err.begin(), Err.end(), double(0));
				x_squared_tot = accumulate(x_squared.begin(), x_squared.end(), double(0));
				Rel_errmean = (errtot / x_squared_tot);

				RK4_ERRORMEANfile << Rel_errmean << "\t" << deltatau << endl;

				//errtotEuler = accumulate(ErrEuler.begin(), ErrEuler.end(), double(0));
				//Rel_errmeanEuler = (errtotEuler / x_squared_t) / n;
				//EULER_ERRORMEANfile << Rel_errmeanEuler << "\t" << deltatau << endl;
			}
		}
		RK4_ERRORMEANfile.close();
		/*EULER_ERRORMEANfile.close();*/
	}
	else
	{
		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(Q) << "_X" << double(start_value) << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), ofstream::app);

		double k1, l1, k2, l2, k3, l3, k4, l4, err, errtot, Rel_errmean, x_squared_tot;

		for (int n = 0; n <= N; ++n)
		{
			k1 = deltatau * f(dX.at(n));
			l1 = deltatau * g(X.at(n), dX.at(n), Q);
			k2 = deltatau * f((dX.at(n) + 0.5*l1));
			l2 = deltatau * g((X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
			k3 = deltatau * f((dX.at(n) + 0.5*l2));
			l3 = deltatau * g((X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
			k4 = deltatau * f((dX.at(n) + l3));
			l4 = deltatau * g((X.at(n) + k3), (dX.at(n) + l3), Q);

			X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));
			dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));
			//x.push_back(X.at(0) * exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));
			x.push_back(exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))));
			x_squared.push_back(pow(exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))), 2));
			Err.push_back(pow((x.at(n) - X.at(n)), 2));

			if (n == N)
			{
				errtot = accumulate(Err.begin(), Err.end(), double(0));
				x_squared_tot = accumulate(x_squared.begin(), x_squared.end(), double(0));
				Rel_errmean = (errtot / x_squared_tot);

				RK4_ERRORMEANfile << Rel_errmean << "\t" << deltatau << endl;
			}
		}
		RK4_ERRORMEANfile.close();
	}
}

void Energy(double Q, double deltatau)
{
	x.clear(); //testing if clearing the vectors will fix Q automation
	X.clear();
	dX.clear();
	x.push_back(2);		//Starting value for ANALYTICAL
	X.push_back(2);     //Starting value for SIMULATION
	dX.push_back(0);    //Starting value for dX	

	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/ENERGY_RK4_DATA_Q" << double(Q) << ".txt";
	ofstream ENERGY_RK4file;
	ENERGY_RK4file.open(ss.str().c_str());

	double k1, l1, k2, l2, k3, l3, k4, l4;
	for (int n = 0; n <= N; ++n)
	{
		k1 = deltatau * f(dX.at(n));
		l1 = deltatau * g(X.at(n), dX.at(n), Q);
		k2 = deltatau * f((dX.at(n) + 0.5*l1));
		l2 = deltatau * g((X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
		k3 = deltatau * f((dX.at(n) + 0.5*l2));
		l3 = deltatau * g((X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
		k4 = deltatau * f((dX.at(n) + l3));
		l4 = deltatau * g((X.at(n) + k3), (dX.at(n) + l3), Q);

		X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));
		dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));
		x.push_back(x.at(0)*exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))));


		ENERGY_RK4file << n << "\t" << (pow(X.at(n), 2) + pow(dX.at(n), 2)) << endl;

	}
	ENERGY_RK4file.close();
}

//void BrownianMotion(double Q, double deltatau)
//{
//	x.clear(); 
//	X.clear();
//	dX.clear();
//
//	x.push_back(0);
//	X.push_back(0);     //Starting value for X
//	dX.push_back(0);    //Starting value for dX
//
//	stringstream ss;
//	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Brownian_Motion/BrownianMotion_Q" << double(Q) << ".txt";
//	ofstream oscillatorRK4file;
//	oscillatorRK4file.open(ss.str().c_str());
//
//	double k1, l1, k2, l2, k3, l3, k4, l4;     //Variables required for Runge-Kutta
//	for (int n = 0; n <= N*10; ++n)
//	{
//		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
//		default_random_engine generator(seed);
//		uniform_real_distribution<double> distribution(0.0, 1.0);
//		U1.push_back(distribution(generator));
//		U2.push_back(distribution(generator));
//		Z0.push_back(sqrt(-2 * log(U1.at(n))) * cos(2 * PI*U2.at(n))); //update array with 1st independent random var
//		Z0.push_back(sqrt(-2 * log(U1.at(n))) * sin(2 * PI*U2.at(n))); //update array with 2nd independent random var
//		
//		//k1 = deltatau * f(dX.at(n));
//		//l1 = deltatau * g(X.at(n), dX.at(n), Q, Z0.at(n));
//		//k2 = deltatau * f((dX.at(n) + 0.5*l1));
//		//l2 = deltatau * g((X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q, Z0.at(n));
//		//k3 = deltatau * f((dX.at(n) + 0.5*l2));
//		//l3 = deltatau * g((X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q, Z0.at(n));
//		//k4 = deltatau * f((dX.at(n) + l3));
//		//l4 = deltatau * g((X.at(n) + k3), (dX.at(n) + l3), Q, Z0.at(n));
//
//		//X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));
//		//dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));
//		//x.push_back(exp(-tau(n, deltatau) / (2 * Q))*(cos(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2)))) + (1 / sqrt(4 * Q*Q - 1))*sin(tau(n, deltatau)*sqrt(1 - (1 / pow((2 * Q), 2))))));
//		//
//		x.push_back() //put new x here
//		oscillatorRK4file << tau(n, deltatau) << "\t" << X.at(n) << endl;
//
//	}
//	oscillatorRK4file.close();
//}
