//#include "All_Funcs.h" 
//#include "Header.h"
#include "RK4.h"
#include <chrono>
using namespace std;

int main()
{	
	////clearcontents(10, 1);
	//auto start = chrono::high_resolution_clock::now();
	////RK4(100, 0.0001);		//RK4 and Euler are functions for a single simulation process (t, X, dX, x)
	////Euler(100, 0.001);
	//
	////for (double Q = 10; Q <= 10000; Q=Q*10)
	////{
	////	Energy(Q, 0.001); //evaluating Energy for different values of Q at dt = 0.001
	////	for (double deltatau = 0.001; deltatau <= 0.064; deltatau = deltatau*2 )
	////		Error(Q, 2, deltatau, false); //error calculations carried out for a range of qs 
	////}
	////
	////
	////for (double start_value = 1; start_value <= 4; start_value++)
	////{
	////	for (double deltatau = 0.001; deltatau <= 0.064; deltatau = deltatau*2)
	////		Error(100.0, start_value, deltatau, true); //error calculations carried out for a range of initial x values
	////}	
	////
	////for (double Q = 5; Q <= 100; Q = Q+15)
	////{
	////	cout << "Q = " << Q << endl;
	////	for (double deltatau = 0.0001; deltatau <= 0.1; deltatau = deltatau+0.0005)
	////		Error(Q, 1, deltatau, false);
	////}
	//for (double Q = 1; Q <= 100; Q = Q * 2)
	//{
	//	BrownianMotion(Q, 0.0001);
	//}
	//	

	//auto end = chrono::high_resolution_clock::now();
	//auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
	//cout << "Execution time: " << elapsed.count() / 1000.0 << " seconds" << endl;

	RK4 oscillator;
	oscillator.set_max_iterations(1000000);

	//oscillator.simulate_oscillations(30, 0.001);
	//oscillator.simulate_energy(30, 0.001);
	//for (double dt = 0.0001; dt < 0.01; dt += 0.0005)
	//{
	//	oscillator.calculate_error(30, dt, 1, false); //set to true if I want to investigate start val
	//}

	//oscillator.simulate_brownian_motion(0.01, 0.001);
	//oscillator.driving_data_for_Bode_Plot(1);

	
	//for (double wD = 0.96; wD < 1.06; wD += 0.01)
	//{
	//	oscillator.simulate_driving(100, 0.001, 1, wD, 1);
	//	oscillator.analytical_driving(100, 0.001, 1, wD, 1);
	//}

	/*for (double driving_freq = 0.95; driving_freq < 1.06; driving_freq += 0.001)
		oscillator.calculate_driving_error(100, 0.0001, 1, driving_freq, 1);*/

	//for (double dt = 0.0001; dt < 0.01; dt += 0.0005)
	//	oscillator.calculate_driving_error(100, dt, 1, 1, 1);

	
	//oscillator.simulate_brownian_motion(50, 0.001);
	//oscillator.simulate_driving_with_noise(100, 0.001, 1, 1, 1);
	
	//for (int i = 0; i < 10; i++)
	//{
	//	for (double Q = 2; Q <= 100; Q = Q++)
	//		oscillator.calculate_X_standard_deviation(Q, 0.001);
	//}

	//for (double dt = 0.0001; dt <= 0.64; dt = dt * 2)
	//	oscillator.calculate_error(30, dt, 2, true);
	
	//for (double w_d = 0.900; w_d <= 0.910; w_d += 0.001)
	oscillator.simulate_NL_driving_with_noise(100, 0.001, 1, 0.8, 0.31); //check starting val for X

	//for (double eta = 0.2; eta <= 0.4; eta = eta + 0.01)
	//{
	//	for (double w_d = 0.9; w_d <= 0.99; w_d = w_d + 0.01)
	//		oscillator.detect_bifurcation(100, 0.001, 1, w_d, eta); //threshold = X*4
	//}

	//for (double w_d = 0.8; w_d <= 0.91; w_d = w_d + 0.001)
		//oscillator.calculate_probability_of_bifurcation(500, 100, 0.001, 1, w_d, 0.31);



	return 0;
}


